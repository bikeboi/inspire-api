# Inspire API
| An API for fetching quotes and verse off the interwebs

### Overview
The inspire-api provieds quotes (and possibly more sometime in the future), scraped off the
internet and serves them in a JSON API.

### Installation
#### Docker
The API has been dockerized and can be accessed from the dockerhub container registry: `docker pull bikeboidev/inspire-api`
The server will run from the port specified in the PORT environment variable. If said variable does not exist. It runs from
port 3001 by default.

#### Stack (Coming soon)
When the project gets published on [hackage](https://hackage.haskell.org/), it will be avaliable for installation using the
[Stack](https://docs.haskellstack.org/en/stable/README/) tool:\
  `stack install inspire-api`

### Documentation
The documentation for the API can be found at [here](inspire-api.readthedocs.io) and soon on Hackage

## License
This project is under an [MIT license](./LICENSE)
