# Fetcher -API Dockerfile

## Install dependencies
FROM haskell:8 as dist

# Build dependencies first
COPY ./app/Setup.hs /dist/build/Setup.hs
COPY ./app/package.yaml /dist/build/package.yaml
COPY ./app/stack.yaml /dist/build/stack.yaml

WORKDIR /dist/build

RUN stack build --only-dependencies

# Add source files and build application
COPY ./app/src /dist/build/src
COPY ./app/exe /dist/build/exe
COPY ./app/test /dist/build/test

RUN stack build inspire-api:exe:release

RUN mv /dist/build/.stack-work/install/x86_64-linux/lts-12.6/8.4.3/bin/release /dist/inspire-bin

FROM node:alpine as node-build

COPY --from=dist /dist/inspire-bin /dist/inspire-bin
COPY app/js /dist/js

WORKDIR /dist/js
RUN npm install

WORKDIR /dist

EXPOSE 80
ENTRYPOINT ["/dist/inspire-bin"]


