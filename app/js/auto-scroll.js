const pup = require ('puppeteer');
const cli = require ('command-line-args')
const redis = require ('redis')
const DB_NUM = 2
const client = redis.createClient({db: DB_NUM})
const {promisify} = require('util')
const fetch = require ('node-fetch')
const http = require('http')

// Promisified redis functions
const getAsync = promisify(client.get).bind(client)
const setAsync = promisify(client.set).bind(client)
const quitAsync = promisify(client.set).bind(client)
const selectAsync = promisify(client.select).bind(client)

const DEFAULT_URL = "http://example.com"
const DEFAULT_PORT = 3001
const DEFAULT_MSG = `
Using default config:
  port: ${DEFAULT_PORT},
  url: ${DEFAULT_URL}
`
const fetch_err_msg = `Could not fetch HTML content.`
function non_v_err(msg) { return msg + `\n(run with --verbose for more information)` }

const options = [
  {
    name: "url", alias: "u",
    type: String
  },
  {
    name: "verbose", alias: "v",
    type: Boolean
  }
]

let getArgs = () => {
  let url
  let verbose
  try {
    const args = cli(options)
    url = args["url"]
    verbose = args["verbose"]
    if (url == undefined) {
      url = DEFAULT_URL
      throw "WARN: UNSPECIFIED URL"  
    }
  } catch(e) {
    console.log("Could not parse cli args\n" + e)
    console.log(DEFAULT_MSG)
  }
  return { url , verbose }
}

const config = getArgs()

function delay(time) {
  return new Promise((res) => setTimeout (res,time))
}

function log_err(msg,e) {
  console.log(msg + "\n<ERR> " + (e ? e : ""))
}

(async () => {
  let url = config['url']
  const browser = await pup.launch({args: ['--no-sandbox']})
  const page = await browser.newPage()
  await page.goto(url)
    .then(function(p) { if (!p.ok()) { throw ("Could not connect to url:'" + url) }})
  const times = 7;
  let content = await page.evaluate(() => new XMLSerializer().serializeToString(document) )  
  console.log("Running automated browser...")
  for(let i = 0; i < times; ++i) {
    await process.stdout.write("\rFetching content: (" + i + "/" + (times-1) + ")")  
    await page.evaluate(() =>  window.scrollBy(0,window.innerHeight * 10))
    let jsonContent = await page.waitForResponse(res => res.request().url() == "https://www.brainyquote.com/api/inf")
        .then(res => res.json()
              .then(obj => obj)
              .catch((err) => "Could not parse response to JSON"))
        .catch((err) => console.log("Response error: " + err))
    content += " " + jsonContent['content']
  } 
  console.log("")
  await browser.close()
  await setAsync(url,content)
    .then((res) => console.log(":: Data storage: OK"))    
    .catch((err) => console.log(":: Data storage: ERROR -> " + err))
  await getAsync(url)
    .then((res) => console.log(":: Data store validation: OK"))
    .catch((err) => console.log(":: Data store validation: ERROR -> \nKEY='" + url + "', VAL=" + err))
  await client.quit()
  await console.log("Work complete. Exiting...")
  await process.exit(0)
})().catch((err) => { 
  config["verbose"] ? log_err(fetch_err_msg,err) : log_err(non_v_err(fetch_err_msg),null)
  process.exit(1)
})
