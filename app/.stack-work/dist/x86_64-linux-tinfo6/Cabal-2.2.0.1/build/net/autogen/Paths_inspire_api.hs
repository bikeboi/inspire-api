{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_inspire_api (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/bikeboi/Documents/Haskell/inspire-api/app/.stack-work/install/x86_64-linux-tinfo6/lts-12.6/8.4.3/bin"
libdir     = "/home/bikeboi/Documents/Haskell/inspire-api/app/.stack-work/install/x86_64-linux-tinfo6/lts-12.6/8.4.3/lib/x86_64-linux-ghc-8.4.3/inspire-api-0.1.0.0-9r4nnf85HKO13xUC9qG9S0-net"
dynlibdir  = "/home/bikeboi/Documents/Haskell/inspire-api/app/.stack-work/install/x86_64-linux-tinfo6/lts-12.6/8.4.3/lib/x86_64-linux-ghc-8.4.3"
datadir    = "/home/bikeboi/Documents/Haskell/inspire-api/app/.stack-work/install/x86_64-linux-tinfo6/lts-12.6/8.4.3/share/x86_64-linux-ghc-8.4.3/inspire-api-0.1.0.0"
libexecdir = "/home/bikeboi/Documents/Haskell/inspire-api/app/.stack-work/install/x86_64-linux-tinfo6/lts-12.6/8.4.3/libexec/x86_64-linux-ghc-8.4.3/inspire-api-0.1.0.0"
sysconfdir = "/home/bikeboi/Documents/Haskell/inspire-api/app/.stack-work/install/x86_64-linux-tinfo6/lts-12.6/8.4.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "inspire_api_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "inspire_api_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "inspire_api_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "inspire_api_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "inspire_api_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "inspire_api_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
