module Util where

import qualified Data.Set as S

noDups :: Ord a => [a] -> [a]
noDups xs = noDups' S.empty xs
  where noDups' _ [] = []
        noDups' s (y:ys) = case y `S.member` s of
                             True -> noDups' s ys
                             False -> y : noDups' (S.insert y s) ys

maybeToEither :: e -> Maybe a -> Either e a
maybeToEither e m = case m of
                      Nothing -> Left e
                      Just x -> Right x

liftMToE :: e -> Either e (Maybe a) -> Either e a
liftMToE e ea = case ea of
                  Left er -> Left er
                  Right Nothing -> Left e
                  Right (Just x) -> Right x
