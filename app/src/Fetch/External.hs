{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

module Fetch.External where

import Fetch.Core
import System.Process
import System.Exit
import Data.List (intercalate)

import Control.Monad.IO.Class
import qualified Data.Text.Lazy as T

data ExtStatus = Success | Failure deriving (Eq, Show)

type CommandSeq = [String]
type ExtProcess = CommandSeq -> IO ExtStatus

mkCmd :: CommandSeq -> String
mkCmd = intercalate " "

runExt :: ExtProcess
runExt cmd = do let s = mkCmd cmd
                code <- waitForProcess =<< spawnCommand s
                case code of
                  ExitSuccess -> return Success
                  ExitFailure _ -> return Failure

withExt :: MonadIO m => CommandSeq -> FetchOpT m a -> FetchOpT m a
withExt cmd fa = do ev <- liftIO $ runExt cmd
                    case ev of
                      Failure -> fetchErr
                                 $ "Could not run external command for fetch operation. CMD:" <> (T.pack . mkCmd) cmd
                      Success -> fa
