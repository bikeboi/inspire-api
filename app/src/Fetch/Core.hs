{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE OverloadedStrings #-}

module Fetch.Core (FetchOpT (..)
                  , MonadFetch, FetchStatus(..)
                  , fetchOk, fetchErr, fetchStatus
                  , liftIO
                  , runFetchOpT
                  , evalFetchOpT
                  , execFetchOpT
                  , liftFetchEither
                  , withRedisCache
                  , fromRedisCache
                  , toRedisCache
                  , delRedisVal
                  , fetchFileBS
                  , (<^>)) where

import Cache.Redis
import Cache.Class

import Control.Applicative
import Control.Monad.Except
import Control.Monad.State as St
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Class (lift)
import Control.Exception
--import Network.Wreq as WR | Removed to reduce build size
--import Control.Lens       | May be re-added later
import qualified Data.Text.Lazy as T
import qualified Data.ByteString.Lazy as BS

-- | Monad fetch class. Abstraction over actions that `fetch` things from
-- | some arbitrary source. Note, `fetchErr` and `fetchOk` are used explicitly
-- | in this module, and it is advised to use them explicitly outside as well,
-- | for the sake of brevity, as they alter the state of the fetch operation.
-- | However, convenience instances have been written for Alternative.

-- Fetch Monad Transformer
class MonadFetch e (m :: * -> *) | m -> e where
  data FetchStatus m :: *
  fetchErr :: e -> m a
  fetchOk :: a -> m a
  fetchStatus :: m a -> m (FetchStatus m)

newtype FetchOpT (m :: * -> *) (a :: *) =
  FetchOpT {  unFetchOpT :: ExceptT T.Text (StateT (FetchStatus (FetchOpT m)) m) a }
  deriving (Functor, Applicative, Monad
           , MonadError T.Text
           ,MonadState (FetchStatus (FetchOpT m)))

instance Monad m => MonadFetch T.Text (FetchOpT m) where
  data FetchStatus (FetchOpT m) = FetchErr | FetchOk deriving (Eq, Show)
  fetchErr = fetchOpTErr
  fetchOk = fetchOpTOk
  fetchStatus = fetchOpTStatus

instance MonadIO m => MonadIO (FetchOpT m) where
  liftIO = lift . liftIO

instance MonadTrans FetchOpT  where
  lift = FetchOpT . lift . lift

-- Convenience Instance
instance Monad m => Alternative (FetchOpT m) where
  empty = throwError "Empty fetch operation"
  (<|>) = (<^>)

runFetchOpT :: Monad m => FetchOpT m a -> m (Either T.Text a, FetchStatus (FetchOpT m))
runFetchOpT x = runStateT (runExceptT (unFetchOpT x)) FetchErr

evalFetchOpT :: Monad m => FetchOpT m a -> m (Either T.Text a)
evalFetchOpT x = evalStateT (runExceptT (unFetchOpT x)) FetchErr

execFetchOpT :: Monad m => FetchOpT m a -> m (FetchStatus (FetchOpT m))
execFetchOpT x = execStateT (runExceptT (unFetchOpT x)) FetchErr

fetchOpTStatus :: Monad m => FetchOpT m a -> FetchOpT m (FetchStatus (FetchOpT m))
fetchOpTStatus x = do s <- lift $ execFetchOpT x
                      case s of
                        FetchErr -> return FetchErr
                        FetchOk -> return FetchOk
  
(<^>) :: Monad m => FetchOpT m a -> FetchOpT m a -> FetchOpT m a
(<^>) fa ga = do fa' <- lift $ evalFetchOpT fa
                 case fa' of
                   Right x -> fetchOk x
                   Left _ -> do ga' <- lift $ evalFetchOpT ga
                                case ga' of
                                  Right x -> fetchOk x
                                  Left e -> fetchErr e
 
-- Implementations of the fetchErr and fetchOk functions
fetchOpTErr :: Monad m => T.Text -> FetchOpT m a
fetchOpTErr e = do St.put FetchErr
                   throwError e

fetchOpTOk :: Monad m => a -> FetchOpT m a
fetchOpTOk a = do St.put FetchOk
                  return a

-- Convenience Functions
liftFetchEither :: MonadFetch e m => Either e a -> m a
liftFetchEither e = case e of
                      Left t -> fetchErr t
                      Right x -> fetchOk x
                      
-- Convenience Fetch functions
-- Useful fetch Interop. (Maybe put this in a separate module) 
withRedisCache :: (MonadIO m,MonadFetch T.Text m) => RedisCacheSettings -> RedisCache a -> m a
withRedisCache s ma = do val <- liftIO $ evalRedisCache s ma
                         case val of
                           Left e -> fetchErr e
                           Right x -> fetchOk x

fromRedisCache :: (MonadIO m,MonadFetch T.Text m) => RedisCacheSettings -> BS.ByteString -> m BS.ByteString
fromRedisCache s k = BS.fromStrict <$> withRedisCache s (cacheGet (BS.toStrict k))

toRedisCache :: (MonadIO m,MonadFetch T.Text m) => RedisCacheSettings -> BS.ByteString -> BS.ByteString -> m ()
toRedisCache s k v = let (ks,vs) = (BS.toStrict k, BS.toStrict v)
                     in withRedisCache s $ cachePut ks vs

delRedisVal :: (MonadIO m,MonadFetch T.Text m) => RedisCacheSettings -> BS.ByteString -> m ()
delRedisVal s k = withRedisCache s $ cacheRemove $ BS.toStrict k

-- IO Embedded in a MonadFetch monad
fetchFileBS :: (MonadIO m, MonadFetch e m) => String -> (SomeException -> e) ->  m BS.ByteString
fetchFileBS s fe = do o <- liftIO $ try $ (BS.readFile s)
                      case o of
                        Right bs -> fetchOk bs
                        Left e -> fetchErr $ fe e


