{-# LANGUAGE OverloadedStrings #-}

module Fetch.Quote where

import Scrape
import Types
import Fetch.Core
import Cache.Redis
import Fetch.External

import Control.Monad ((>=>))
import Text.HTML.Scalpel.Core (scrapeStringLike)
import Data.List (intercalate)
import qualified Data.ByteString.Lazy.Char8 as B8
import qualified Data.ByteString.Lazy as BS
import Data.Hashable

type Url = String
type Domain = String

-- | Quote Fetching

-- Things that should probably be in a config file
htmlDB :: Integer
htmlDB = 2

scrollScript :: String
scrollScript = "./js/auto-scroll.js"

quoteDomain :: String
quoteDomain = "http://www.brainyquote.com"

scrollCmd :: Url -> CommandSeq
scrollCmd url = ["node",scrollScript,"--url",url,"-v"]

-- This should stay hardcoded probably
allQuotesKey :: BS.ByteString
allQuotesKey = B8.pack . show $ hashWithSalt 7 ("all-quotes-key" :: BS.ByteString)

-- Fetching the quotes
htmlCacheSettings :: RedisCacheSettings
htmlCacheSettings = noPersist htmlDB

fetchQuoteHTML :: QuoteTopic -> FetchOpT IO BS.ByteString
fetchQuoteHTML topic = do url <- return $ intercalate "/" [quoteDomain,getEP topic]
                          let urlKey = B8.pack url
                          fromRedisCache htmlCacheSettings urlKey
                            <^> withExt (scrollCmd url) (fromRedisCache htmlCacheSettings urlKey)

extractQuotes :: BS.ByteString -> FetchOpT IO [QuoteData]
extractQuotes bs = do scr <- return $ scrapeStringLike bs quoteScraper
                      case scr of
                        Nothing -> fetchErr failMsg
                        Just [] -> fetchErr failMsg
                        Just x -> fetchOk x
  where failMsg = "Could not scrape quotes from HTML doc"

quotesFromData ::[QuoteData] -> FetchOpT IO [Quote]
quotesFromData = return . (map fromData)

fetchQuotes :: QuoteTopic -> FetchOpT IO [Quote]
fetchQuotes = fetchQuoteHTML >=> extractQuotes >=> quotesFromData
