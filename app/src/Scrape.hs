{-# LANGUAGE OverloadedStrings #-}

module Scrape where

import Types

import Text.HTML.Scalpel.Core
import Data.Text.Lazy.Encoding
import Control.Monad.State
import qualified Data.Text.Lazy as T
import qualified Data.ByteString.Lazy as BS

-- Quote Scraping(from brainyquote.com)
selectQList :: Selector
selectQList = ("div" @: ["class" @= "clearfix"])

data QPart = Auth | Quot

aAttr :: AttributeName -> T.Text -> Selector
aAttr a s = "a" @: [a @= T.unpack s]

selectQPart :: QPart -> Selector
selectQPart Auth = selectQList // ("a" @: ["title" @= "view author"])
selectQPart Quot = selectQList // ("a" @: ["title" @= "view quote", hasClass "b-qt"])

type QuoteScraper = StateT [T.Text] (Scraper T.Text) 

quoteScraper :: Scraper BS.ByteString [QuoteData]
quoteScraper = do qs <- texts $ selectQPart Quot
                  as <- texts $ selectQPart Auth
                  return $ zipWith QuoteData (decodeUtf8 <$> as) (decodeUtf8 <$> qs)
          
-- Little Util here for collecting the versions. Ignore it, but I might need it later
{-
versionList :: Selector
versionList = "select" @: ["name" @= "version_download"] // "option"

cleanVersion :: BS.ByteString -> BS.ByteString
cleanVersion s = let rvrs = BS.reverse s
                     last5 = BS.take 5 rvrs
                 in case last5 of
                      "2scu_" -> BS.reverse . (BS.drop 5) $ rvrs
                      _ -> s

versions :: Scraper BS.ByteString [BS.ByteString]
versions = do vs <- attrs "value" versionList
              return $ cleanVersion <$> vs

eval :: IO ()
eval = do vs <- scrapeURL "https://unbound.biola.edu/index.cfm?method=downloads.showDownloadMain" versions
          case vs of
            Nothing -> putStrLn "Could not fetch Versions"
            Just x -> do BS.writeFile "versions.txt" (BS.intercalate "\n" x) 
                         putStrLn "Version Collection: OK"
-}

-- Mood Scraping
selectMood :: Selector
selectMood = "div" @: ["class" @= "column4"] // "a"

moodScraper :: Scraper BS.ByteString [BS.ByteString]
moodScraper = attrs "href" selectMood

selectVerseMeta :: Selector
selectVerseMeta = "div" @: ["class" @= "bibleChapter2"] // "a"

verseMetaScraper ::  Scraper BS.ByteString [BS.ByteString]
verseMetaScraper = texts selectVerseMeta
