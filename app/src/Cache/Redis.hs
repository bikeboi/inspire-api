{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Cache.Redis where

import Cache.Class

import Control.Monad.Except
import Control.Monad.State
import Control.Monad.Reader
import Database.Redis as Red
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.Encoding as TE
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString as BSS

data RedisCacheSettings =
  RedisCacheSettings { _persist :: Bool
                     , _db :: Integer }

withPersist :: Integer -> RedisCacheSettings
withPersist = RedisCacheSettings True

noPersist :: Integer -> RedisCacheSettings
noPersist = RedisCacheSettings False

fromCacheSettings :: RedisCacheSettings -> Red.ConnectInfo
fromCacheSettings RedisCacheSettings { _db = d } = defaultConnectInfo { connectDatabase = d }

newtype RedisCache a =
  RedisCache { runRedisCache :: CacheT (ReaderT Connection IO) a }
  deriving (Functor, Applicative, Monad
           ,MonadState CacheState
           ,MonadReader Connection
           ,MonadIO
           ,MonadError T.Text)

evalRedisCache :: RedisCacheSettings -> RedisCache a -> IO (Either T.Text a) 
evalRedisCache cs@(RedisCacheSettings { _persist = p }) a = do
  conn <- Red.connect (fromCacheSettings cs)
  val <- runReaderT (evalCacheT (runRedisCache a)) conn
  case p of
    True -> runRedis conn (Red.bgrewriteaof) >> return val
    False -> return val

redisConn :: RedisCacheSettings -> IO Connection
redisConn = Red.connect . fromCacheSettings

emptyVal :: BS.ByteString
emptyVal = ""
            
instance MonadRedis RedisCache where
  liftRedis r = do conn <- ask
                   liftIO $ runRedis conn r

instance MonadCache RedisCache BSS.ByteString where
  type Val RedisCache = BSS.ByteString
  cacheState = cacheState

  cacheGet k = handleGet =<< (liftGet <$> (liftRedis $ Red.get k)) -- :: RedisCache (Either Reply (Maybe BSS.ByteString)))
    where handleGet v = case v {-:: Either T.Text BSS.ByteString-} of
                          Left e -> withMiss $ throwRedisErr $ e <> ", KEY: " <> bssToT k
                          Right x -> withHit $ return x

  cachePut k v = do o <- liftRedis $ Red.set k v
                    case o of
                      Left _ -> withMiss $ throwRedisErr "Unable to store value in database"
                      Right _ -> return ()

  cacheRemove k = do b <- liftRedis $ Red.exists k
                     case b of
                       Left _ -> withMiss $ throwRedisErr $ "Unable to check if key exists: KEY: " <> bssToT k
                       Right True -> withHit (liftRedis (Red.del [k])) >> return ()
                       Right False -> withMiss $ throwRedisErr $ "Key does not exist. KEY: " <> bssToT k


-- Helper(s)
liftGet :: Either Reply (Maybe BSS.ByteString) -> Either T.Text BSS.ByteString
liftGet ea = case ea of
               Left rep -> Left $ handleRedisReply rep
               Right Nothing -> Left "Value not found in database"
               Right (Just x) -> Right x

bssToT :: BSS.ByteString -> T.Text
bssToT = TE.decodeUtf8 . BS.fromStrict

throwRedisErr :: T.Text -> RedisCache a
throwRedisErr = throwError . redisPrompt

redisPrompt :: T.Text -> T.Text
redisPrompt = mappend "REDIS:"

handleRedisReply :: Reply -> T.Text
handleRedisReply = redisPrompt . handleReply
  where handleReply :: Reply -> T.Text
        handleReply (SingleLine s) = TE.decodeUtf8 $ BS.fromStrict s
        handleReply (Error s) = TE.decodeUtf8 $ BS.fromStrict s
        handleReply (Integer x) = (T.pack . show) x
        handleReply (Bulk Nothing) = "Bulk reply: Nothing"
        handleReply (Bulk (Just s)) = TE.decodeUtf8 $ BS.fromStrict s
        handleReply (MultiBulk Nothing) = "MultiBulk reply: Nothing"
        handleReply (MultiBulk (Just s)) = T.pack . show . (map handleRedisReply) $ s 
