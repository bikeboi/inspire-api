{-# LANGUAGE FunctionalDependencies,
             TypeFamilies,
             KindSignatures,
             GeneralizedNewtypeDeriving,
             MultiParamTypeClasses,
             FlexibleContexts #-}

module Cache.Class where

import Control.Monad.Except
import Control.Monad.State
import qualified Data.Text.Lazy as T

data CacheState = CacheHit | CacheMiss deriving (Eq, Show)

class (MonadState CacheState m) => MonadCache m (k :: *) | m -> k where
  type Val m :: *

  cacheGet :: k -> m (Val m)
  cachePut :: k -> Val m -> m ()
  cacheRemove :: k -> m ()
  cacheState :: m CacheState

-- Generic StateT wrapper for caching monads (instances are declared elsehwere)
type CacheT m a = ExceptT T.Text (StateT CacheState m) a
runCacheT :: CacheState -> CacheT m a -> m (Either T.Text a, CacheState)
runCacheT s a = runStateT (runExceptT a) s 

execCacheT :: Monad m => CacheT m a -> m CacheState
execCacheT a = snd <$> runCacheT CacheHit a

evalCacheT :: Monad m => CacheT m a -> m (Either T.Text a)
evalCacheT a = fst <$> runCacheT CacheHit a

-- Helper
withHit :: MonadCache m k => m a -> m a
withHit ma = do put CacheHit
                ma

withMiss :: MonadCache m k => m a -> m a
withMiss ma = do put CacheMiss
                 ma
