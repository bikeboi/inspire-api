{-# LANGUAGE OverloadedStrings #-}

module Types where

import Data.Text.Lazy as T
import Data.Aeson hiding (Success)
import Data.Aeson.Encoding (lazyText)
import Control.Monad.Except
import Data.Hashable

-- Useful monad and utility functions
type EitherIO = ExceptT T.Text IO
runEitherIO :: EitherIO a -> IO (Either T.Text a)
runEitherIO = runExceptT

reEitherIO :: IO (Either T.Text a) -> EitherIO a
reEitherIO e = do e' <- liftIO e
                  liftEither e'

liftMaybe :: MonadError e m => e -> Maybe a -> m a
liftMaybe t Nothing = throwError t
liftMaybe _ (Just a) = return a

-- For QuoteTopics
class IsEndpoint a where
  getEP :: a -> String
--

-- Quote Types
data Quote = Quote Int QuoteData
  deriving (Eq, Show)

instance Ord Quote where
  (Quote x _) `compare` (Quote y _) = x `compare` y

instance ToJSON Quote where
  toJSON (Quote i qd) =
    object ["hash" .= i, "quote-data" .= qd]

instance Hashable Quote where
  hashWithSalt _ (Quote x qd) = hashWithSalt x qd

type Author = T.Text
type Content = T.Text

data QuoteData =
  QuoteData Author Content
  deriving (Eq, Show)

instance ToJSON QuoteData where
  toJSON (QuoteData a t) =
    object ["author" .= a, "content" .= t]

instance Hashable QuoteData where
  hashWithSalt s (QuoteData a t) = hashWithSalt s (a <> t)

data QuoteTopic
  = Motivational
  | Life
  | Science
  | Wisdom
  | Friendship --
  | Success
  | Art
  | God
  | Faith
  | Knowledge
  | Environmental --
  deriving (Eq, Ord)

instance FromJSON QuoteTopic where
  parseJSON = withText "topic" parseTopic
    where parseTopic t = case topicFromText $ T.fromStrict t of
                           Left e -> fail $ T.unpack e
                           Right x -> return x


instance FromJSONKey QuoteTopic where
  fromJSONKey = FromJSONKeyTextParser $ parseTopic
     where parseTopic t = case topicFromText $ T.fromStrict t of
                           Left e -> fail $ T.unpack e
                           Right x -> return x

instance ToJSONKey QuoteTopic where
  toJSONKey = ToJSONKeyText (T.toStrict . T.pack . show) (lazyText . T.pack . show)

instance ToJSON QuoteTopic where
  toJSON = String . T.toStrict . T.pack . show

instance IsEndpoint QuoteTopic where
  getEP x = t <> show x
    where t = "topics/"

instance Show QuoteTopic where
  show Motivational = "motivational"
  show Life = "life"
  show Science = "science"
  show Wisdom = "wisdom" 
  show Friendship = "friendship"
  show Success = "success"
  show Art = "art"
  show God = "god"
  show Faith = "faith"
  show Knowledge = "knowledge"
  show Environmental = "environmental"

topicFromText :: T.Text -> Either T.Text QuoteTopic
topicFromText t = case t of
                    "motivational" -> pure Motivational
                    "life" -> pure Life
                    "science" -> pure Science
                    "wisdom" -> pure Wisdom
                    "friendship" -> pure Friendship
                    "success" -> pure Success
                    "art" -> pure Art
                    "god" -> pure God
                    "faith" -> pure Faith
                    "knowledge" -> pure Knowledge
                    "environmental" -> pure Environmental
                    _ -> Left $ "Topic: '" <> t <> "' does not exist"

allTopics :: [QuoteTopic]
allTopics = [Motivational, Life, Science, Wisdom
            ,Friendship,Success,Art,God,Faith,Knowledge,Environmental]

-- Useful (pure) functions with quotes
fromData :: QuoteData -> Quote
fromData qd = Quote (hash qd) qd

-- Eliminates the title of the quote from the content
fixQuoteText :: QuoteData -> QuoteData
fixQuoteText (QuoteData a t) = QuoteData a $ T.takeWhile (/= '-') t 

-- // --
-- // --
