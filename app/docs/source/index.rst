.. inspire-api documentation master file, created by
   sphinx-quickstart on Wed Aug 29 20:40:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Inspire API Documentation
=========================

   | Something something... inspire.
   | - Anonymous

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   overview
   api-spec
   architecture
   future


* :ref:`search`
