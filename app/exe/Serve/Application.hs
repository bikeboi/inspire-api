module Serve.Application where

import Network.Wai.Handler.Warp
import Serve.API

runApp :: IO ()
runApp = do putStrLn "Running fetcher server"
            runEnv 3001 inspireApp
