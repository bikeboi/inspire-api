{-# LANGUAGE FlexibleInstances #-}

-- | Token class and all its instances

module Serve.Token where

import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as BS8
import qualified Data.Text.Lazy as T
import qualified Data.Text as TS
import qualified Data.Text.Lazy.Encoding as TE
import Data.Aeson

class (Ord a, FromJSON a, ToJSON a) => IsToken a where
  toToken :: a -> BS.ByteString

instance IsToken T.Text where
  toToken = TE.encodeUtf8

instance IsToken TS.Text where
  toToken = TE.encodeUtf8 . T.fromStrict

instance IsToken String where
  toToken = BS8.pack 
