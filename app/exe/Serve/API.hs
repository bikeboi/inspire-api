{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Serve.API where

import Serve.Handlers
import Serve.API.Quote
import Serve.API.Session

import qualified Data.Text.Lazy as T
import Servant

-- Quote API(s)
type InspireAPI = "inspire" :>
                  (
                    QuoteSessionAPI
                    :<|> QuoteAPI
                    :<|> Get '[JSON] (JT T.Text)
                  )

inspireAPI :: Proxy InspireAPI
inspireAPI = Proxy

inspireServer :: Server InspireAPI
inspireServer = quoteSessionServer :<|> quoteServer :<|> root

inspireApp :: Application
inspireApp = serve inspireAPI inspireServer

