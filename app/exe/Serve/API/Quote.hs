{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Serve.API.Quote where

import Servant
import Serve.Handlers
import Fetch.Core
import Fetch.Quote
import Types
import JSONTemp
import qualified Data.Text.Lazy as T

type QuoteAPI = "quotes" :>
                (
                  Get '[JSON] (JT [QuoteTopic])
                  :<|> QuoteTopicAPI
                )

quoteAPI :: Proxy QuoteAPI
quoteAPI = Proxy

quoteServer :: Server QuoteAPI
quoteServer = quoteTopics :<|> quoteTopicServer

type QuoteTopicAPI = Capture "topic" T.Text :>
                   (
                     QueryParam "from" Int :> QueryParam "take" Int :> Get '[JSON] (FmtOut [Quote])
                     :<|> Get '[JSON] (FmtOut [Quote])
                   )

quoteTopicAPI :: Proxy QuoteTopicAPI
quoteTopicAPI = Proxy

quoteTopicServer :: Server QuoteTopicAPI
quoteTopicServer t = sliceRange t :<|> quotesByTopic t

-- Handlers
quoteTopics :: Handler (JT [QuoteTopic])
quoteTopics = return . JSONTemp Ok $ allTopics

quotesByTopic :: T.Text -> Handler (FmtOut [Quote])
quotesByTopic t = do case topicFromText t of
                       Left e -> jtemp404 e
                       Right top -> do qs <- liftIO $ evalFetchOpT $ fetchQuotes top
                                       case qs of
                                         Left e -> jtemp404 e
                                         Right qs' -> liftFmtOut qs'

sliceRange :: T.Text -> Maybe Int -> Maybe Int -> Handler (FmtOut [Quote])
sliceRange tp frm to = do qs <- quotesByTopic tp
                          return $ mapFmtOut (maybeTake to . maybeDrop frm) qs




