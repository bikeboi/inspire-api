{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Serve.API.Session where

import Types
import Serve.API.Quote (quotesByTopic)
import Serve.Session.Types
import Serve.Session.Quote
import Servant
import Serve.Handlers
import qualified Data.Text.Lazy as T

import Control.Monad.IO.Class (liftIO)

-- Quote Session
type QuoteSessionAPI = "user" :> Capture "token" T.Text :>
                       (
                         Capture "topic" T.Text :> QueryParam "take" Integer :> Get '[JSON] (FmtOut [Quote])
                         :<|> ReqBody '[JSON] QuoteSesh :> Post '[JSON] (FmtOut QuoteSesh)
                         :<|> Delete '[JSON] (FmtOut T.Text) 
                         :<|> Get '[JSON] (FmtOut QuoteSesh)
                       )

quoteSessionAPI :: Proxy QuoteSessionAPI
quoteSessionAPI = Proxy

--Helper
indexWrap :: Integer -> [a] -> [a]
indexWrap _ [] = []
indexWrap i xs = let ind = fromIntegral i
                     n = ind `mod` length xs 
                 in [xs !! n]

incrWrap :: Integer -> Integer -> Integer
incrWrap l i = case i >= l of
                  True -> 0
                  False -> i + 1


quoteSessionServer :: Server QuoteSessionAPI
quoteSessionServer tkn = quoteNext tkn :<|> postSession tkn :<|> deleteUser tkn :<|>  showSession tkn

quoteNext :: T.Text -> T.Text -> Maybe Integer -> Handler (FmtOut [Quote])
quoteNext tkn tpc tk = do qs <- quotesByTopic tpc
                          let len = case unwrapFmtOut qs of
                                      A x -> fromIntegral $ length x
                                      E _ -> 200  -- Inaccessbile code. RIP
                          v <- liftIO $ evalSessionT tkn $ getQSeshVal' tpc
                          case v of
                            Left e -> jtemp404 e
                            Right x -> do _ <- liftIO $ evalSessionT tkn $ updateQuoteSesh' (incrWrap len) tpc
                                          case tk of
                                            Nothing -> return $ mapFmtOut (indexWrap x) qs
                                            Just t -> return $ mapFmtOut (take' t . drop' x) qs
-- More helpers
take' :: Integer -> [a] -> [a]
take' x = take (fromIntegral x)

drop' :: Integer -> [a] -> [a]
drop' x = drop (fromIntegral x)

postSession :: T.Text -> QuoteSesh -> Handler (FmtOut QuoteSesh)
postSession tkn qs = do _ <- liftIO $ evalSessionT tkn $ saveQSesh qs
                        test <- liftIO $ evalSessionT tkn $ getQSesh
                        case test of
                          Left e -> jtemp404 $ "Could not retrieve saved session :-> " <> e
                          Right sh -> if sh == qs
                                      then liftFmtOut sh
                                      else jtemp404 "Store inconsistency. Could not update user session"

deleteUser :: T.Text -> Handler (FmtOut T.Text)
deleteUser t = do v <- liftIO $ evalSessionT t $ deleteQSesh
                  case v of
                    Left e -> jtemp404 e
                    Right _ -> liftFmtOut $ "Successfully deleted user session. Token: " <> t

showSession :: T.Text -> Handler (FmtOut QuoteSesh)
showSession tkn = do sh <- liftIO $ evalSessionT tkn $ getQSesh
                     case sh of
                       Left e -> jtemp404 e
                       Right x -> liftFmtOut x

