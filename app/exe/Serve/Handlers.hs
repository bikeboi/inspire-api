{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}

module Serve.Handlers where

import JSONTemp
import Servant
import Servant.Server.Internal.ServantErr (ServantErr (..)) -- For making errors with JSON templates
import Control.Monad.Except (throwError)

import qualified Data.Text.Lazy as T
import qualified Data.ByteString as BSS -- Strict bytestrings used when generating responses
import qualified Data.ByteString.Lazy as BS
import Data.Aeson

type JT = JSONTemp
type FmtOut a = JT (AOrErr a)

unwrapFmtOut :: FmtOut a -> AOrErr a
unwrapFmtOut (JSONTemp _ x) = x

liftFmtOut :: (ToJSON a,Monad m) => a -> m (FmtOut a)
liftFmtOut = return . fmtOk

liftFmtOut' :: (ToJSON a,Monad m) => T.Text -> m (FmtOut a)
liftFmtOut' = return . fmtErr

fmtOk :: ToJSON a => a -> FmtOut a
fmtOk = JSONTemp Ok . A

fmtErr :: T.Text -> (FmtOut a)
fmtErr = JSONTemp Err . E

fmtOkBS :: ToJSON a => a -> BSS.ByteString
fmtOkBS = BS.toStrict . encode . fmtOk

fmtErrBS :: T.Text -> BSS.ByteString
fmtErrBS t = BS.toStrict . encode $ (fmtErr t :: FmtOut T.Text)

mapFmtOut :: (a -> b) -> FmtOut a -> FmtOut b
mapFmtOut = fmap . fmap

data AOrErr a = A a | E T.Text

instance Functor AOrErr where
  fmap f (A a) = A . f $ a
  fmap _ (E e) = E e 

instance ToJSON a => ToJSON (AOrErr a) where
  toJSON (A a) = toJSON a
  toJSON (E t) = toJSON t

jtempErr :: ServantErr -> T.Text -> Handler (FmtOut a)
jtempErr se fm = throwError
                 $ se { errHeaders = [("content-type","application/json")], errBody = encode (fmtErr fm :: FmtOut T.Text) }

-- Specializations
jtemp404 :: T.Text -> Handler (FmtOut a)
jtemp404 = jtempErr err404

root :: Handler (JT T.Text)
root = return . JSONTemp Err $ "Invalid Endpoint. See documentation for available endpoints"

-- Helpers for pagination
maybeSlice :: (Int -> [a] -> [a]) -> Maybe Int -> [a] -> [a]
maybeSlice _ Nothing  xs = xs
maybeSlice f (Just x) xs = f x xs

maybeTake :: Maybe Int -> [a] -> [a]
maybeTake = maybeSlice take

maybeDrop :: Maybe Int -> [a] -> [a]
maybeDrop = maybeSlice drop
