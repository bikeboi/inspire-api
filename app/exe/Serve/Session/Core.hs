{-# LANGUAGE OverloadedStrings #-}

module Serve.Session.Core
  (sessionCacheSettings
  ,updateSesh
  ,getSesh
  ,getSeshVal
  ,saveSesh
  ,deleteSesh) where

import Fetch.Core
import Cache.Redis
import Serve.Session.Types
import Data.Aeson (ToJSON,FromJSONKey,ToJSONKey,decode,encode)
import Control.Monad.Trans.Class (lift)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (ask)
import Control.Monad.Except (liftEither)

import qualified Data.Map as M
import qualified Data.Text.Lazy.Encoding as TE
import qualified Data.ByteString.Lazy as BS

-- Interface for session functions
-- Concfiguration stuff (Should be in a config file)
sessionCacheSettings :: Integer -> RedisCacheSettings
sessionCacheSettings = withPersist

getSeshJSON :: RedisCacheSettings -> SessionT IO BS.ByteString
getSeshJSON s = do tkn <- ask
                   js <- liftIO $ evalFetchOpT $ fromRedisCache s tkn 
                   case js of
                     Left e -> lift $ fetchErr $ "Could not fetch user :-> " <> e
                     Right x -> return x

decodeSesh :: (Ord k,FromJSONKey k,ToJSONKey k) => BS.ByteString -> SessionT IO (Sesh k)
decodeSesh bs = case decode bs of
                  Nothing -> lift $ fetchErr $ "Could not parse user session string. Output: " <> TE.decodeUtf8 bs
                  Just s -> lift $ fetchOk $ Sesh s

getSesh :: (Ord k,FromJSONKey k, ToJSONKey k) => RedisCacheSettings -> SessionT IO (Sesh k)
getSesh s = getSeshJSON s >>= decodeSesh

getSeshVal :: (Ord k,ToJSON k) => Integer -> k -> Sesh k -> SessionT IO Integer
getSeshVal i k (Sesh sm) = let v = M.lookup k sm
                           in case v of
                              Nothing -> (return $ M.insert k i sm) >> (lift $ fetchOk i)
                              Just x -> lift $ fetchOk x

alterSesh :: (Ord k, ToJSON k) => (Integer -> Integer) -> k -> Sesh k -> SessionT IO (Sesh k)
alterSesh f k sh@(Sesh sm) = do v <- getSeshVal 0 k sh
                                lift . fetchOk $ Sesh $ M.insert k (f v) sm

saveSesh :: ToJSONKey k => RedisCacheSettings -> Sesh k -> SessionT IO ()
saveSesh s (Sesh sm) = do tkn <- ask
                          (=<<) liftEither $ evalFetchOpT $ toRedisCache s tkn (encode sm)

deleteSesh :: RedisCacheSettings -> SessionT IO ()
deleteSesh s = do tkn <- ask
                  lift $ delRedisVal s tkn 

-- Gets, updates and stores. The whole shabang
updateSesh :: (Ord k, ToJSON k,ToJSONKey k, FromJSONKey k)
           => RedisCacheSettings
           -> (Integer -> Integer) -> k
           -> SessionT IO (Sesh k)
updateSesh s f k = do sh <- getSesh s >>= alterSesh f k
                      saveSesh s sh
                      return sh
                  
