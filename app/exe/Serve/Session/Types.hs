{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Serve.Session.Types
  (AES.encode
  ,SessionMap
  ,Sesh (..)
  ,SessionT
  ,runSessionT
  ,evalSessionT) where

import Serve.Token
import Fetch.Core
import Control.Monad.Reader
import qualified Data.Text.Lazy as T
import qualified Data.Map.Lazy as M
import qualified Data.ByteString.Lazy as BS
import Data.Aeson as AES

type SessionMap k = M.Map k Integer

data Sesh (k :: *) where
  Sesh :: (FromJSONKey k,ToJSONKey k) => SessionMap k -> Sesh k 

instance Eq k => Eq (Sesh k) where
  (Sesh sm) == (Sesh sm') = sm == sm'

instance (Ord k,ToJSONKey k) => ToJSON (Sesh k) where
  toJSON (Sesh sm) = object ["session" .= sm ]

instance (Ord k,FromJSONKey k,ToJSONKey k) => FromJSON (Sesh k) where
  parseJSON = withObject "sesh" $ \v -> Sesh <$>
    v .: "session"

-- Session Monad
type SessionT m a = ReaderT BS.ByteString (FetchOpT m) a
runSessionT :: (Monad m, IsToken t) => t -> SessionT m a -> m (Either T.Text a, FetchStatus (FetchOpT m))
runSessionT t s = runFetchOpT (runReaderT s $ toToken t) 

evalSessionT :: (Monad m, IsToken t) => t -> SessionT m a -> m (Either T.Text a)
evalSessionT t s = evalFetchOpT (runReaderT s $ toToken t)
