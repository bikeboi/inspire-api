module Serve.Session.Quote where

import Fetch.Core (fetchErr)
import Serve.Session.Core
import Serve.Session.Types
import Cache.Redis (RedisCacheSettings)
import Types
import Control.Monad.Trans.Class (lift)
import qualified Data.Text.Lazy as T

type QuoteSesh = Sesh QuoteTopic

quoteSeshDB :: Integer
quoteSeshDB = 4

quoteSessionSettings :: RedisCacheSettings
quoteSessionSettings = sessionCacheSettings quoteSeshDB

getQSesh :: SessionT IO QuoteSesh
getQSesh = getSesh quoteSessionSettings

getQSeshVal :: QuoteTopic -> SessionT IO Integer
getQSeshVal q = getSesh quoteSessionSettings >>= getSeshVal 1 q

getQSeshVal' :: T.Text -> SessionT IO Integer
getQSeshVal' t = case topicFromText t of
                   Left e -> lift $ fetchErr e
                   Right x -> getQSeshVal x

saveQSesh :: QuoteSesh -> SessionT IO ()
saveQSesh = saveSesh quoteSessionSettings

deleteQSesh :: SessionT IO ()
deleteQSesh = deleteSesh quoteSessionSettings

updateQuoteSesh :: (Integer -> Integer) -> QuoteTopic -> SessionT IO QuoteSesh
updateQuoteSesh = updateSesh quoteSessionSettings

updateQuoteSesh' :: (Integer -> Integer) -> T.Text -> SessionT IO QuoteSesh
updateQuoteSesh' f t = case topicFromText t of
                         Left e -> lift $ fetchErr e
                         Right x -> updateQuoteSesh f x
