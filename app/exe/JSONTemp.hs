{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE KindSignatures #-}

module JSONTemp (JSONTemp (..), FetchStatus (..))where

import Data.Aeson
import qualified Data.Text as T

data FetchStatus = Ok | Err deriving (Eq, Show)

instance Semigroup FetchStatus where
  (<>) Err _ = Err
  (<>) _ Err = Err
  (<>) x _   = x

instance ToJSON FetchStatus where
  toJSON Ok  = toJSON ("OK" :: T.Text)
  toJSON Err = toJSON ("ERROR" :: T.Text)

instance FromJSON FetchStatus where
  parseJSON = withText "status" (\t -> statFromText <$> return t) 
    where statFromText "OK" = Ok
          statFromText _ = Err

data JSONTemp a
  = JSONTemp FetchStatus a
  deriving (Eq, Show)

instance (ToJSON a) => ToJSON (JSONTemp a) where
  toJSON (JSONTemp fs a) = object ["status" .= fs,"content" .= a]

instance FromJSON a => FromJSON (JSONTemp a) where
  parseJSON = withObject "JsonTemp" $ \v -> JSONTemp
    <$> v .: "status"
    <*> v .: "content"

instance Functor JSONTemp where
  fmap f (JSONTemp s a) = JSONTemp s $ f a
