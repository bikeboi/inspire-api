module Main where

import Serve.Application

main :: IO ()
main = do
  putStrLn "Starting service..."
  -- Initialize moods from config
  runApp
  
