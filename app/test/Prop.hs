{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Prop where

import Test.QuickCheck
import Test.QuickCheck.Monadic
import Control.Monad.Identity
import qualified Data.Text.Lazy as T
import Fetch.Core
import Fetch.Quote

instance (Arbitrary a,Monad m, Eq a) => Arbitrary (FetchOpT m a) where
  arbitrary = arbFetch
    where arbFetch = do a <- (arbitrary :: Gen a)
                        oneof [return $ fetchOk a,
                               return $ fetchErr "Error fetch"]

instance Show a => Show (FetchOpT Identity a) where
  show fa = case runIdentity (runFetchOpT fa) of
              (Left e,s) -> "FETCH ERROR: MSG=" <> T.unpack e
              (Right a,s) -> "FETCH OK: VAL=" <> show a

prop_fetchOpAlt :: (Eq a,Arbitrary a) => FetchOpT Identity a -> FetchOpT Identity a -> Property
prop_fetchOpAlt f g = monadic runIdentity $ do
  (fa,fs) <- run $ runFetchOpT f
  (ga,gs) <- run $ runFetchOpT g
  (sol,s) <- run $ runFetchOpT $ f <^> g
  case (fs,gs) of
    (FetchErr, FetchOk) -> assert $ sol == ga && s == gs
    _ -> assert $ sol == fa && s == fs

type AltPropTest a = FetchOpT Identity a -> FetchOpT Identity a -> Property

data TestOp t =
  TestOp { _onStart :: String
         , _prop :: t
         , _onEnd :: String }

altPropTest :: TestOp (AltPropTest Int)
altPropTest = TestOp { _onStart = "Running FetchOP Alt test.." 
                     , _prop = prop_fetchOpAlt
                     , _onEnd = "Complete FetchOp Alt test" }
              
runTest :: Testable t => TestOp t -> IO ()
runTest TestOp {_onStart = s, _prop = p, _onEnd = e } = do
  putStrLn s
  quickCheck p
  putStrLn e

main :: IO ()
main = do putStrLn "Initializing test suite"
          runTest altPropTest

